Come hither to this place and let your mind fly free
For that's the only way how everyone should be
We all need something that we can keep on livin' for
No one needs to tell us what we're doin' anymore

Head on, head on steady - until the end of time
Living, living ready - for livin' ain't no crime

So tell me, aren't we all just one holding the flame
Against al the babbits in this world who build up frames
Some time or tomorrow we will prove them traitors wrong
So make your dreams come true for it mustn't take too long

Head on, head on steady - until the end of time
Living, living ready - for livin' ain't no crime

Now what's the use of livin here and what's it worth
Don't ask but throw away your fear, enjoy this earth
You are here to live don't think you're here to understand
Don't forget you're no one without someone's helping hand

Head on, head on steady - until the end of time
Living, living ready - for livin' ain't no crime