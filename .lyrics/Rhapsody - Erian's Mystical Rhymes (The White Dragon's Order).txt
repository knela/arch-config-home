In primordial times of sorrow
Ancient ages of the known world
Kron's reign of terror was rising
So the Skylords sent their angels
Against the storm that treathened heavens
Seven wars were so fought
The dead rose
Led by the Darklords of Hell

The last war
Meant the end
Of hellgod's
Old reign

Dark memories lost in the shades of illusion
Warn the light of electric sky
The angels could change the life on earth
But not the demons' old plans

United force of strong wise men...
A new order soon was born
Ten kings, ten wizards, elves and dragonlords
Ready to fight, to fight for their world

Ready to face the last secret
Hidden for 5000 angel's years
5000 years

A great and new hope in Elgard was born
The one hope that could finally find the...

Sacred words to stop the dark plan of Nekron
All contained in Erian's Mystical White Book

The holy council cast their vote
Hearing the deep voice of soul
Etherus... Iras... two names on all
Elected to face the darklord

They knew so well Erian's tale
What could be found between his rhymes
His rhymes of divine light
Written to save our lands

Ready to face the last secret
Hidden for 5000 angel's years
5000 years

Now is the time for them to share all their plans
The one hope that could finally find the...

Sacred words to stop the dark plan of Nekron
All contained in Erian's Mystical White Book

So the name was chosen:
White Dragon's Order
All sworn to fight
All for one
One for all

A great and new hope in Elgard was born
The one hope that could finally find the...

Sacred words to stop the dark plan of Nekron
All contained in Erian's Mystical White Book
Sacred words to stop the dark plan of Nekron
All contained in Erian's Mystical White Book... White Book...

(The White Dragon's Order)

(Iras:)
"And so the Order of the White Dragon decreed Khaas the hero of the middle lands.
He and princess Lothen from the kingdom of the ancient waterfalls,
together with Tharish, elven king of the caverns and myself, Iras Algor from Hor-Lad,
would travel the path of incredible danger and great adventure.

Dargor the shadowlord would lead us through the ancient underworld of Dar-Kunor.
Finally the time had come to find the legendary seventh black book,
that containing the worst of all the known prophecies"