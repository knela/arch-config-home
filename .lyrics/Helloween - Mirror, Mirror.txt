My age you keep uncertain
This spell cannot be broken
Visions of eternal youth
We share one face together
Associates forever
Only you know the truth

Mirror mirror on the wall
Whose the master of them all?
Mirror Mirror split in two
Look at me, who are you?

I swim in my reflection
Mirror answer my question
Tell me what I want to hear
I see a perfect creature
Contracted human feature
I'll drown you with your vanity

Mirror Mirror on the wall
Whose the sickest of them all
Mirror Mirror split in two
Look at me - who are you?

No, my life is beautiful
I said, my life is beautiful

Mirror Mirror on the wall
Whose the sickest one of all
Mirror mirror split in two
Look at me - Who are you
Mirror Mirror on the wall
Whose the master of them all
Mirror Mirror split in two
Look at me - who are you?