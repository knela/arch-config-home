C'era una volta un uomo
Con gli occhi verso la luna
E si chiedeva:
"Verra presto l'amore?"
E che altro esiste
In un cuore gelato?
Tranne il pensiero
Di un assassinio molto silenzioso