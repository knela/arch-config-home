See into the future, see into the past
I gotta tell you what I'm seeing in the glass
Tall dark stranger knocking at your door
Looking through the window, it's you he's looking for
Don't be acting crazy, don't you cause a riot
Stand very still, keep very quiet

You'll never see the face of the man in the window
Heart begins to race
He's the one to spring you a surprise
Aah, the Ripper, master of disguise

See into the mind, see into the brain
Try to find the reason that Jacky's out again
Slipping and sliding, don't even try to hide
Just like your shadow breathing at your side
Don't give into panic, don't you run and scream
Aah, the Ripper, haunting all your dreams

You'll never see the face of the man in the window
Heart begins to race
He's the one to spring you a surprise
Aah, the Ripper, master of disguise

Cold steel, whisper in the night
He'll be at your side with a smile and a knife
It's seems like dreaming, moving in the dance
The last embrace you'll ever know, the violence of romance

Don't try to run, you'll trip and fall
You'd be a fool
He's right beside you
And he can be so cruel

Stand, oh, very still
Your heart is beating like a drum
He turns his face towards you
And the two move as one

And so the mystery continues to beguile
The ones who know can never tell you of his smile
See the faces shiver, see the figures move
How can you see? They move so fast
You're bound to lose