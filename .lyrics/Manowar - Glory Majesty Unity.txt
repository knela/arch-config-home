Deep into the heart of the battle they fought.
Covered on all sides as all converged on them,
until the four could no longer be seen.
As time passed I feared them lost,
then slowly the armies separated,
many were dead.
I saw the four
each down on one knee,
all stopped to watch and gaze upon them with a smile of victory
before sending them into the ground.

Then they rose together
to make a final stand.
With their last bit of strength
they raised their arms into the air
pointing blood stained weapons to the sky.
They called upon the god of war and made ready to die.
But Odin would not call them this day to Valhalla,
instead he sent thunder and lightning
to strike the ground, bestowing upon them the
One gift every warrior lives in hope of - the Berserker rage!

Now filled with that strength,
the power of a thousand men was given them.
No longer mortal
they were touched by the gods.
This time when they took up the attack,
men fell not by tens,
but by hundreds,
by thousands.
And when the smoke did clear,
the four spoke the words
and the masses answered the response of the warrior's prayer.

Gods of War I call you
My sword is by my side
I seek a life of honor
Free from all false pride

I will crack the whip
With a bold mighty hail
Cover me with death
If I should ever fail

Glory, majesty, unity
Hail! Hail! Hail!