[Hansi Kürsch:] Is it too late to prevent their fall?
[Bob Catley:] Are they ready for the key to it all?
[Hansi Kürsch:] The mystery solved, the answer to life
[Bob Catley:] The final solution, a chance to survive

[Bob Catley And Hansi Kürsch:]
We can save this ill-fated race
Who are lost in the ocean of space
Show them the way to reverse their decline
Guide them back on the river of time

[Hansi Kürsch:] Follow the wave, speed up the flight
[Bob Catley:] Slow down time, faster than light
[Hansi Kürsch:] Unite the forces, take a giant leap
[Bob Catley:] Bend the stream, dive into the deep

[Bob Catley And Hansi Kürsch:]
We can save this ill-fated race
Who are lost in the ocean of space
Show them the way to reverse their decline
Guide them back on the river of time

[Hansi Kürsch:] Send back visions of war and decay
[Bob Catley:] Paradigms of fear in a world of dismay
[Hansi Kürsch:] Shape the present, alter the past
[Bob Catley:] Create a new future, one that would last

[Bob Catley And Hansi Kürsch:]
We can save this ill-fated race
Who are lost in the ocean of space
Show them the way to reverse their decline
Guide them back on the river of time