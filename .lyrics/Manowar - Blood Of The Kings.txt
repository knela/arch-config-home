Brothers, the battle is raging
Choose your side
Sing with us the battle hymn
Into glory ride
Hail to England
The sign of the hammer's our guide
Forever we're fighting the world
Side by side

On a crusade
The world we bring
Four kings of metal
Four metal kings
Death to the false ones
Dance on a string
'Til the blood on your sword is the blood of a king
'Til the blood on your hand is the blood of a king

Our armies in England, Ireland, Scotland, and Wales
Our brothers in Belgium, Holland and France will not fail
Denmark, Sweden, Norway, Finland, Italy
Switzerland, Austria
Back to the glory of Germany

On a crusade
The world we bring
Four kings of metal
Four metal kings
Death to the false ones
Dance on a string
'Til the blood on your sword is the blood of a king
'Til the blood on your hand is the blood of a king (king, king)

Sound the death tone
On our march for revenge
Spill the blood of my enemies
The oath of a friend
Fight the holy war for the crown and the ring
Six magic circles made by the blood of the kings

Wherever we hide, it's metal we bring
Four kings of metal
Four metal kings
Chant of true metal
We will sing
'Til the blood on your sword is the blood of a king
'Til the blood on your hand is the blood of a king

Sound the death tone
On our march for revenge
Spill the blood of my enemies
The oath of a friend
Fight the holy war for the crown and the ring
Six magic circles were made by the blood of the kings

Wherever we ride, it's metal we bring
Four kings of metal
Four metal kings
Death to the false ones
Dance on a string
'Til the blood on your sword is the blood of a king
'Til the blood on your hand is the blood of a king
'Til the blood on your sword is the blood of a king
'Til the blood on your hand is the blood of a king
'Til the blood on your sword is the blood of a king
Blood of a king
Blood of a king