CHEMIST
The 'Frame is running all our systems,
No way to shut it down
Outcries of panic in the distance,
Fear... all around

DIPLOMAT
No satellites, no radio, no TV,
I can't even use my phone
We need to wake up from this bad dream,
We're not alone!

PROPHET
I've seen the future in a dream
I've seen a sea of machines
I've seen a realm beneath the waves
Another time, another space

CHEMIST: The 'Frame is closing all
The channels, isolated from the world
DIPLOMAT: But there's still time to heal
The damage and lift this curse

PROPHET
I've seen the future in a dream
I've seen a sea of machines
I've seen a realm beneath the waves
Another time, another space

COUNSELOR
You need to reach beyond your fear
You need to stand your ground and fight
If we're to make it out of here
If we're to leave Alpha alive

CAPTAIN
I can sail us to the skies 
I can fly us to the stars
I won't allow our race to die
I'll never leave you in the dark

PRESIDENT
I'll find a way to save our kind
After all I am to blame
Just follow me, and I will guide
And I'll release us from the 'Frame

CHEMIST: The 'Frame is cutting off
The power, we're stranded in the dark
DIPLOMAT: It won't help to hide away
Like cowards, make a brand new start!

PROPHET
I've seen the future in a dream
I've seen a sea of machines
I've seen a realm beneath the waves
Another time, another space

I see a comet cleave the sky
In a time beyond time
I see a realm beneath the waves
I see a castle, a castle deep in space
On the edge of time!