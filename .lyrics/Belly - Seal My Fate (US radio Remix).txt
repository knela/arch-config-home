Untame my brain.
Give me a name.
Hear my faith.
Seal my fate.
Unholy and dirty words I gathered to me,
Thinking the point was keep what's mine for me,
While he's laughing.
Hear my faith.
Seal my fate.
And when you breathe, you breathe for two.
And if you think you've finally found the perfect light,
I hope it's true.
Unholy and dirty words I gathered to me,
Thinking the point was keep what's mine for me,


While he's laughing.
On every track I fractured every back,
Thinking the point was step on every crack,
And he's laughing.
Hear my faith.
Seal my fate.
And when you breathe, you breathe for two.
And if you think you've finally found the perfect light,
I hope it's true.
You say you saw him laughing.
I hope it's true.
I'd like to see it happen. I hope it's true
'cause I can feel it.
I hope it's true.