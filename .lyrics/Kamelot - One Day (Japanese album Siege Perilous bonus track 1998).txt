can you remember the morning 
I told you goodbye
just when a new was dawning
a piece of me died

somehow I have to try to
getting used to being alone
If I could only hold you once again
I'd never let go

save me a prayer 
when day turns to night
lord won't you 
show me the light

one day - I'll find back to my heart and soul again
one day - I swear swear by your name

memories keep me awake
and you're there by my side
I wonder if its only in my mind 
that I am alive

save me a prayer 
when day turns to night
lord won't you 
tell me I'm right

one day - I'll find back to my heart and soul again
one day - I swear swear by your name

one day - I'll find back to my heart and soul again
one day - I swear swear by your name X2