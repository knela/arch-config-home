Bo
[Spoken]
Love songs used to be so beautiful...
"Let us go then, you and I,
When the evening is spread out against the sky,
Like a patient etherized upon a table."
-T.S. Eliot
Nowadays, thanks to corporately-owned pop stars, love songs are even more beautiful. How beautiful are today's love songs?
I'll show you.

[Sung]
I love your hair, I love your name, I love the way you say it
I love your heart, and you're so smart, 'cause you gave away it
I live your sis, I love your dad, I love your mum
But more than all of that I love the fact that you are dumb- enough
(Swag!)

To not realize everything I've said has been said before
In a thousand ways, in a thousand songs
Sung with the same four chords!
But you'll still love it and let me finger you
(Finger you?)
Yeah, finger you!

Satan
FINGER YOU!

Bo
chorus 1
Oh girl, I hope you don't think that I'm rude
When I tell you that I love you, boo
I also hope that you don't see through
This cleverly constructed ruse
Designed by a marketing team
Cashing in on puberty and low self esteem
(So low!)
And girls' desperate need to feel loved
(Love! Love, love, love, love)
(Please love me!)

America says we love a chorus
But don't get complicated and bore us
Though meaning might be missin'
We need to know the words after just one listen, so

Repeat stuff, repeat stuff, repeat stuff
Repeat stuff, repeat stuff, repeat stuff
Repeat stuff, repeat stuff, repeat stuff
Repeat stuff, yeah
(Repeat stuff! Yeah, repeat stuff!)

I love my baby and you know I couldn't live without her
But now I need to make every girl think this song's about her
Just to make sure that they spread it like the plague
So I describe my dream girl as really, really vague- like

I love your hands, 'cause your fingerprints are like no other
I love your eyes and their blueish, brownish, greenish color
I love it when you smile that you smile wide
And I love how your torso has an arm on either side- now

If you're my agent
You might be thinking,
Oh no,
Sound the alarms!
You're not appealing
To little girls who don't have arms
But they can't use iTunes, so
Fuck 'em, who needs 'em?

Bo
chorus 1

Satan
I am a servant of darkness
I am the Void
The rivers shall run red with the blood of virgins
I take many shapes
This is one of them
The strong will be made weak
And the weak shall bow before me
...SWAG!

Bo
America says we love a chorus
But don't get complicated and bore us
Though meaning might be missin'
We need to know the words after just one listen, so-

Repeat stuff, repeat stuff, repeat stuff
Repeat stuff, repeat stuff, repeat stuff
(Repeat stuff for me, stuff!)
Repeat stuff, repeat stuff, repeat stuff
Repeat stuff, repeat stuff, repeat stuff
(Stuff, gay, gay stuff)
Repeat stuff, repeat stuff, repeat stuff
Repeat stuff, repeat stuff, repeat stuff
Repeat stuff, repeat stuff, repeat stuff
Repeat stuff, yeah
(Yes! Repeat it! Repeat it till the day you die!)

I'm in magazines
Full of model teens
So far above...you
So read them and hate yourself
Then pay me to tell you I love you
(I love you)

And your parents will always come along
Because their little girl is in love
And how could love be wrong?
How could love be wrong-

When you repeat stuff, repeat stuff, repeat stuff
Repeat stuff, repeat stuff, repeat stuff
(Unh, yeah, fuck, fucking repeat it again)
Repeat stuff, repeat stuff, repeat stuff
(Repeat it again, yeah, keep repeating it)
Repeat stuff, repeat stuff, repeat stuff
[Excerpt from Adolf Hitler's speech at the Nuremberg Rally]
Repeat stuff, repeat stuff, repeat stuff
[Giggling]
Repeat stuff, repeat stuff, repeat stuff
Repeat stuff, repeat stuff, repeat stuff
Repeat stuff, YEAH!

We know it's not right
We know it's not funny
But we'll stop beating this dead horse
When it stops spittin' out money
But until then
We will repeat stuff.