A young cowboy named Billy Joe
Grew restless on the farm
A boy filled with wanderlust
Who really meant no harm
He changed his clothes and shined his boots
And combed his dark hair down
And his mother cried as he walked out;

Refrain:
"Don't take your guns to town, son
Leave your guns at home, Bill
Don't take your guns to town."

He sang a song as on he rode,
His guns hung at his hips
He rode into a cattle town,
A smile upon his lips
He stopped and walked into a bar and laid his money down
But his mother's words echoed again;

Refrain:
"Don't take your guns to town, son
Leave your guns at home, Bill
Don't take your guns to town."

He drank his first strong liquor then to calm his shaking hand
And tried to tell himself at last he had become a man
A dusty cowpoke at his side began to laugh him down
And he heard again his mother's words;

Refrain:
"Don't take your guns to town, son
Leave your guns at home, Bill
Don't take your guns to town."

Bill was raged and Billy Joe reached for his gun to draw
But the stranger drew his gun and fired before he even saw
As Billy Joe fell to the floor the crowd all gathered 'round
And wondered at his final words;

Refrain:
"Don't take your guns to town, son
Leave your guns at home, Bill
Don't take your guns to town."