We live in a binary reality.  We do.
It's a world of black and white.
There's only two types of people in this world:
Those who can finish lists

What is art? You know.  What is art?
Is art something gay people do to get back at their fathers? .... Could be.

What is an artist?
What makes a great artist? You know.  .... Great artists, like myself,
Like the great director Michael Bay,
Like the great Irish actor Shaquille O'Neal?

We ask questions.
You know, questions nobody else dares to ask
Questions like
Where are all the Sour Patch Parents?
Questions like if Mickey's a mouse, and Minnie's a mouse, and Donald's a duck, and Daisy and Goofy, if they're all animals and they can talk
Why is Pluto just a fucking dog?
Did they just forget to anthropomorphize him or, worse, is Mickey keeping him mentally handicapped to stay a pet? 
I'm not, how does that fit in to that universe, that paradigm
Goofy's a dog, he's talking
This one, crawling around
Guys, you got tight, I would never bash Disney
Never
Ever
I think Disney teaches girls
Young girls such important lessons that princess fairy tales, you know like
Cinderella: It doesn't matter where you come from or how poor you are, you know, as long as you're incredibly hot
Snow White: Which encourages children to
You know, give midgets nicknames
Sleeping Beauty: You know, which encourages, um
Date rape, maybe not
Maybe not that one

I was doing a show recently on the border of, uh, Hannah Montana and South Dakota Fanning
And after the show
After the show, a guy came up to me
And said, "Bo, why don't you ever tell stories on-stage about people coming up to you after the shows?"
And I said, "'Cause they're never funny"

I've always wanted a black girlfriend
Not as, you know, 'cause like
I don't know, h-how, how do I put this?
When we 69, I can call it "yin-yanging"
Um
It's not a, it's not racist
Guys, it's 21st Century racism
It's racism in light of itself
The only reason I'm saying these things
Is because the stigmas about race are already there
And I'm just playing off of that
And they understand that
So, if, after the show,
You see like a black guy beating me up,
He's doing it ironically, okay?
Get your story straight