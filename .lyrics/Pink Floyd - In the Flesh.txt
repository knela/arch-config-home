...we came in? [1]

So ya thought ya might like to go to the show
To feel the warm thrill of confusion, that space cadet glow
Tell me is something eluding you sunshine?
Is this not what you expected to see?
If you want to find out what's behind these cold eyes
You'll just have to claw your way through this disguise

Lights!
Roll the sound effects!
Action!
Drop it!
Drop it on 'em!
Drop it on 'em! [2]