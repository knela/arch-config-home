We've been doing a lot of laughing
Which is good, uh, for a comedy show on a comedy CD, but what we haven't been doing is a lot of thinking
And I'd like to do that now, I've written some haikus
Haikus are Japanese poems consisting of 17 syllables, three lines
Five, seven, five
And I find them to have a certain
Philosophical construct, there's a certain, uh
Soundness in their simplicity, a clearness in their cogency, if you will
So hopefully what we'll do right now is read these haikus, think for a bit
And then when we go back, uh, to the
You know, the jokes and the laughing
They'll have benefited, uh, from the time we took to think
So um, you guys just sit back and indulge me and just think for a bit and then we'll go back to the jokes
Uh, can I get some blue light to set the mood?
Perfect
For those of you listening on CD, the lights didn't change which made it funny

I saw a rainbow
On the day my grandma died
Fuckin' lesbian
(Ding)

For fifteen cents a
Day you can feed an African
They eat pennies
(Ding)

Old peoples' skin sags
Because it's being pulled toward
The underworld
(Ding)

Do unto others
As you would have them do to you
Said the rapist
(Ding)

My aunt used to say
Slow and steady wins the race
She died in a fire
(Ding)

Even if he is
Your friend, never, ever call
An Asian person
(Ding)

And finally

Bono, if you want
To help poor people, sell your
Tinted shades, you cunt
(Ding)

Thank you, this next piece is called "Sonnet 155", or "If Shakespeare Had Written a Porn", and it goes like this
I saw the morning dew betwixt thine thighs
As I removed my source of Grecian power
As if King Midas dared to touch the skies
Upon thy body fell a golden shower

Thy body's temples, two church bells had rung
Upon thy chest, a row of pearls bestowed
The sun had set, thy set with wary hung
I thought, "How black a night and blue a lode"

I said, "What light through yonder beaver breaks?
It is the yeast"

And now my belly's yellow
My pole gives cause to storms and earthy quakes
But 'tis not massive, I am no Othello

And when that final moment came to pass
Like Christ I came-a riding on an ass
Thank you very much

William Shakespeare, uh
William Shakespeare was a verbal cun-tortionist
He could bend his words in the way a contortionist bends his frame without hope that he could with a name like William Shakespeare
William Shakespeare, some, some of you seem lost, look
Say your name was Robert Frost and you couldn't write, that would suck
Well, I guess you could always go as Bobby Frost and own an ice cream truck
He was balanced like a simile and could stack metaphor five, six at a time and rhyme into the very last line of a soliloquy which finally said outright with a previous 77 rolling hinting at
He had puns and quips and tons of trips of sons with ships with nuns with hips and buns and lips, but I had something that Shakespeare never had
Penicillin
See, it hadn't been invented yet, back then they only had "quill"-icillin
Hey, it's not that hard, bard
I'm sorry, I got a bone to pick with you, William
So if you could just listen up here and listen to this theater queer's theater query here and maybe act like a real artist for once in your life
Say Van Gogh, and
Lend me your ear
You're not a writer
You're a writer like fucking Hulk Hogan's a street fighter
You write these dramas
You accumulate your wealth
You hold nature as to a mirror of yourself
Just because you're messed up doesn't mean we are too
Just because you want to bang your mom doesn't mean Danish princes do, what
Who? Yeah, Hamlet, Shakespeare, that's right, the young prince whose father died at the hands of his uncle with whom his mother lied, sound familiar?
It's the fucking Lion King
You stole from a Disney movie, you androgynous douche, what's next
The story of a French king on a quest to find his lost son, Nemo?
Oh, and by the way, poetic talent is really easy to fake when thy sentences doth no fucking sense make

"To be, or not to be
That is the question, whether 'tis nobler in the mind to suffer the slings and arrows of outrageous fortune
Or to take arms against a sea of troubles and by opposing end them? To die
To sleep, no more, and by a sleep to say we end the heartache and the thousand natural shocks that flesh is heir to
'Tis a consummation devoutly to be wished. To die, to sleep
To sleep, perchance to dream, ay
There's the rub, for in that sleep of death what
Dreams may come when we have shuffled off this mortal coil, must give us pause"
Pft, like what?

This next song is about quantum mechanics
(Hits random keys on the piano)
This next song....

I was raised very well, like a field of corn
You know, I was also raised very Christian, like the Children of the Corn
And Christians get angry at me 'cause I say things like, "Why the long nose, Pope-nocchio?"
They'll think I'll go to Hell
The truth is, I've been to Christian Hell
And I actually wrote a song about it

Hitler was there
And so were all the Jews, yeah
So it got a little awkward